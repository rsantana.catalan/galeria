<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmailController extends Controller{

    public function email(Request $request){

        request()->validate([
            'nombre'=>'required',
            'email'=>'required',
            'mensaje'=>'required'
        ]);
        
        $to_name = 'Sebastian Fuentes';
        $to_email = 'sebastian.fuentes75@gmail.com';
        $from_email = $request->email;

        $data = array('name'=>$request->nombre, 'body' => $request->mensaje, 'from' => $request->email);

        Mail::send('email.contactemail', $data, function($message) use ($to_name, $to_email, $from_email){
            $message->to($to_email, $to_name)->subject('Contacto Galeria');
            $message->from($from_email,'Galeria');
        });
        return redirect('/');
    }
}
