<?php

namespace App\Http\Controllers;

use App\fotografias;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Image;



class FotografiasController extends Controller{

    public function index(){

        $fotografias = DB::table('fotografias')->latest()->limit(12)->get();
        return view('fotografias.index',['fotografias'=>$fotografias]);
    }

    public function get($id){
        $fotografia = DB::table('fotografias')
                    ->select('id','titulo','descripcion','lugar','lat','lng','tipo_id','url_thumbnail')
                    ->find($id);

        $tipos = DB::table('tipo')->select('id','nombre')->get();

        

        return json_encode(array('fotografias'=>$fotografia,'tipos'=>$tipos));
    }

    public function gallery(){

        $fotografias = DB::table('fotografias')
                            ->join('tipo','fotografias.tipo_id','=','tipo.id')
                            ->select('fotografias.*','tipo.nombre')
                            ->paginate(12);
                            

        $tipos = DB::table('fotografias')
                            ->join('tipo','fotografias.tipo_id','=','tipo.id')
                            ->groupBy('nombre')
                            ->select('nombre')
                            ->get();
       
        return view('fotografias.galeria',['tipos'=>$tipos],['fotografias'=>$fotografias]);
    }

    public function createThumbnail($path){

        $filename = $path->getClientOriginalName();
       
        $img = Image::make($path)->resize(1280,720,function($constraint){
            $constraint->aspectRatio();
        });
        
        $thumbnail = Image::make($img)->crop(450,450);

        $path_thumbnail = public_path('/storage/thumbnail/'.$filename);
        
        $path_thumbnail_short = $filename;
        $thumbnail->save($path_thumbnail);
        return $path_thumbnail_short;
    }

    public function createImage($path){

        $filename = $path->getClientOriginalName();
       
        $img = Image::make($path)->resize(1960,1080,function($constraint){
            $constraint->aspectRatio();
        });
        $path_image = public_path('/storage/pictures/'.$filename);
        
        $path_image_short = '/pictures/'.$filename;
        $img->save($path_image);
        return $path_image_short;
    }

    public function show($id){

        $fotografia = DB::table('fotografias')->find($id);
        $animales = fotografias::find($id)->animales; // pueden ser mas de uno
        $lugar = fotografias::find($id)->lugares;
       
        $data['fotografia'] = $fotografia;
       
        
        return view('fotografias.show',$data);

    }
    
    public function create(){
        
        
        $data['tipos'] = DB::table('tipo')->select('id','nombre')->get();
        
        return view('fotografias.create', $data);

    }

    public function store(Request $request){

        request()->validate([
            'titulo'=>'required',
            'descripcion'=>'required',
            'mapa'=>'required',
            'lugar'=>'required',
            'tipo'=>'required',
            'fotografia'=>'required'
        ]);

        $fotografia = new Fotografias();
        $fotografia->titulo = $request->titulo;
        $fotografia->descripcion = $request->descripcion;
        $fotografia->lugar = $request->lugar;
        $fotografia->lat = $request->lat;
        $fotografia->lng = $request->lng;
        $url_thumb = $this->createThumbnail($request->file('fotografia'));
        $fotografia->url_thumbnail = $url_thumb;
        $url_file = $this->createImage($request->file('fotografia'));
        $fotografia->url_fotografia = $url_file;  
        $fotografia->tipo_id = $request->tipo;
        $fotografia->administrador_id = Auth::id();
        
        $fotografia->save();
    
        return redirect('/');
    }

    public function edit(Request $request){

        $id = $request->id;
        $old_fotografia = $request->old_fotografia;
        $new_titulo = $request->titulo;
        $new_descripcion = $request->descripcion;
        $new_lugar = $request->lugar;
        $new_lat = $request->lat;
        $new_lng = $request->lng;
        $new_tipo = $request->tipo;
        
        if(($request->file('fotografia')) != NULL){

            Storage::disk('public')->delete('/pictures/'.$old_fotografia);
            Storage::disk('public')->delete('/thumbnail/'.$old_fotografia);

            $url_thumb = $this->createThumbnail($request->file('fotografia'));
            $url_file = $this->createImage($request->file('fotografia'));

            $fotografia = DB::table('fotografias')
                ->where('id','=',$id)
                ->update(['titulo'=>$new_titulo,'descripcion'=>$new_descripcion,'lugar'=>$new_lugar,'lat'=>$new_lat,'lng'=>$new_lng,'url_fotografia'=>$url_file,'url_thumbnail'=>$url_thumb,'tipo_id'=>$new_tipo]);

        }else{
            $fotografia = DB::table('fotografias')
                ->where('id','=',$id)
                ->update(['titulo'=>$new_titulo,'descripcion'=>$new_descripcion,'lugar'=>$new_lugar,'lat'=>$new_lat,'lng'=>$new_lng,'tipo_id'=>$new_tipo]);
        }

        /*$fotografia = DB::table('fotografias')
                ->where('id','=',$id)
                ->update(['titulo'=>$new_titulo,'descripcion'=>$new_descripcion,'lugar'=>$new_lugar,'url_fotografia'=>$url_file,'url_thumbnail'=>$url_thumb,'regiones_id'=>$new_region
                ,'tipo_id'=>$new_tipo]);*/
                
        return 1;
    }

    public function administrar(){
        
        $fotografias = DB::table('fotografias')
                ->join('tipo','fotografias.tipo_id','=','tipo.id')
                ->select('fotografias.*','tipo.nombre')
                ->paginate(12);
        

        $tipos = DB::table('fotografias')
                ->join('tipo','fotografias.tipo_id','=','tipo.id')
                ->groupBy('nombre')
                ->select('nombre')
                ->get();

        return view('fotografias.administrar',['tipos'=>$tipos],['fotografias'=>$fotografias]);
        
    }

    public function delete(Request $request){
        
        Storage::disk('public')->delete('/pictures/'.$request->picture);
        Storage::disk('public')->delete('/thumbnail/'.$request->picture);

        DB::table('fotografias')
            ->where('id','=',$request->id)->delete();

        
        return redirect('/administrar');
    }
}
