<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFotografiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotografias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo',100);
            $table->string('descripcion',300);
            $table->string('lugar',100);
            $table->string('lat',100);
            $table->string('lng',100);
            $table->string('url_fotografia',300);
            $table->string('url_thumbnail',300);
            $table->timestamps();
        });

        Schema::table('fotografias', function (Blueprint $table) {

            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')
                    ->references('id')
                    ->on('tipo')
                    ->onDelete('cascade');

            $table->bigInteger('administrador_id')->unsigned();
            $table->foreign('administrador_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fotografias');
    }
}
