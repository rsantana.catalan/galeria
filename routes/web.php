<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'FotografiasController@index');
Route::get('/create','FotografiasController@create')->middleware('auth');
Route::post('/create/store','FotografiasController@store')->middleware('auth');
Route::get('/galeria','FotografiasController@gallery');
Route::get('/administrar', 'FotografiasController@administrar')->middleware('auth');
Route::delete('/administrar/delete/{id}','FotografiasController@delete')->middleware('auth');
Route::get('/adminstrar/get/{id}','FotografiasController@get');
Route::post('/adminstrar/edit/{id}','FotografiasController@edit');
Route::get('/email/contactemail','EmailController@email');

Auth::routes(['register'=>false]);
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');