<!-- Contact -->
<section id="contact">
<!-- Social -->
    <div class="social column">
        <h3>Sobre Mi</h3>
        <p>Estamos pereciendo frente a la tecnología moderna ante sus infinitos estímulos visuales, sonidos e información. Distinguimos alarmas de un despertador, conocemos las voces de personas que admiramos por la TV, absorbemos conocimientos de una pantalla, nuestras manos sujetan constantemente aparatos electrónicos (incluso mientras reflexiono lo hago escribiendo en una computadora), nos hemos limitado a las figuras de pantalla, a las distancias y los datos. Pero la naturaleza, las aves, los insectos, la flora, los ríos, el mar y la tierra siguen al alcance nuestro, sólo si lo queremos ver.</p>
        <p>Espero inspirarte usando estas imágenes, escucha el silencio, desconéctate y sal a buscar lo que has perdido. Asómbrate de nuevo con lo simple.</p>
        <p>Mi nombre es Sebastián y te invito a ver lo que he visto.</p>
        <h3>Sigueme</h3>
        <ul class="icons">
            <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
            <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
            <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
        </ul>
    </div>

<!-- Form -->
    <div class="column">
        <h3>Contactame</h3>
        <form action="/email/contactemail" method="get">
            <div class="field half first">
                <label for="nombre">Nombre</label>
                <input name="nombre" id="nombre" type="text" placeholder="Nombre">
            </div>
            <div class="field half">
                <label for="email">Email</label>
                <input name="email" id="email" type="email" placeholder="Email">
            </div>
            <div class="field">
                <label for="mensaje">Mensaje</label>
                <textarea name="mensaje" id="mensaje" rows="6" placeholder="Mensaje"></textarea>
            </div>
            <ul class="actions">
                <li><input value="Enviar Mensaje" class="button" type="submit"></li>
            </ul>
        </form>
    </div>

</section>