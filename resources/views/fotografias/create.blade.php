@php 
    $incluirBanner = false;
    $incluirContact = false;
    $incluirFooter = false;
@endphp

@extends ('layout')

@section ('contenido')

<div class="title">
<h1>Agregar Fotografia</h1>
</div>
<form action="/create/store" enctype="multipart/form-data" method="POST">
    <div id="columnaCrear">
        <div  class="columnAgregar">
            @csrf
            <div class="field">
                <label for="titulo">Titulo: </label>
                <input type="text" name="titulo" value="{{ old('titulo') }}">
                @if($errors->has('titulo'))
                    <p class="help is-danger">{{ $errors->first('titulo') }}</p>
                @endif 
            </div>
            <div class="field">
                <label for="descripcion">Descripción: </label>
                <input type="text" name="descripcion" value="{{ old('descripcion') }}">
                @if($errors->has('descripcion'))
                    <p class="help is-danger">{{ $errors->first('descripcion') }}</p>
                @endif
            </div>
            <div class="field">
                <label for="tipo">Tipo: </label>
                <select class="select-wrapper" name="tipo">
                    
                    <option value="">-</option>
                    @foreach($tipos as $tipo)
                        @if (old('tipo') == $tipo->id)
                            <option selected="selected" value="{{$tipo->id}}"> {{$tipo->nombre}}</option>
                        @else
                            <option value="{{$tipo->id}}"> {{$tipo->nombre}}</option>
                        @endif
                    @endforeach
                </select>
                @if($errors->has('tipo'))
                    <p class="help is-danger">{{ $errors->first('tipo') }}</p>
                @endif
                
            </div>
            <div class="field">
                <label for="fotografia">Fotografia: </label>
                <input type="file" name="fotografia">
            </div>

            <div class="actions">
                <input type="submit" value="Crear Foto">
            </div>
        </div>
        <div class="columnAgregar">
            <div class="field">
                <label for="mapa">Lugar</label>
                <input id="mapa" type="text" name="mapa" value="{{ old('mapa') }}">
                <input id="lugar" name="lugar" value="" type="hidden">
                <input id="lat" name="lat" value="" type="hidden">
                <input id="lng" name="lng" value="" type="hidden">
                @if($errors->has('descripcion'))
                    <p class="help is-danger">{{ $errors->first('mapa') }}</p>
                @endif
            </div>
            <div id="map"> 
            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTOIRO9iDITo9D-IG8q6ouGETHVfx7_ug&callback=initMap&libraries=places&language=es&region=CL" async defer></script>
<script>	
    var map;
  	function initMap(){
        map = new google.maps.Map(document.getElementById('map'),{
		  center: {lat: 0, lng: 0},
          zoom: 2,
        });
        
        var input = document.getElementById('mapa');
        var searchBox = new google.maps.places.Autocomplete(input);
        var marker = new google.maps.Marker();

        google.maps.event.addListener(searchBox, 'place_changed', function(){

            marker.setMap(null);
            var place = searchBox.getPlace();
            console.log(place)
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            var latLng = {lat:lat, lng:lng};
            var lugar = place.name;
        
            marker = new google.maps.Marker({
                
                position: latLng,
                map: map,
                
            });

            map.setZoom(8);
            map.setCenter(marker.getPosition());

            document.getElementById('lugar').setAttribute('value',lugar);
            document.getElementById('lat').setAttribute('value',lat);
            document.getElementById('lng').setAttribute('value',lng);
        });
    } 
</script>
@endsection

