
@php 
    $incluirBanner = false;
    $incluirContact = true;
    $incluirFooter = true;
@endphp

@extends ('layout')

@section ('contenido')
<header id="header">
    <span>Sebastian Fuentes</span>
</header>
<section id="galleries">
    <div class="gallery">
        <header>
            <h1>Galeria</h1>
            <ul class="tabs">
                <li><a href="#" data-tag="Todas" class="button">Todas</a></li>
                @foreach($tipos as $tipo)
                    <li><a href="#" data-tag="{{$tipo->nombre}}" class="button"> {{$tipo->nombre}}</a></li>
                @endforeach
            </ul>
        </header>

        <div class="content">
        @foreach ($fotografias as $fotografia)
            <div class="media Todas {{$fotografia->nombre}}">
                <a href="/storage/{{$fotografia->url_fotografia}}">
                    <img src="{{ asset('storage/thumbnail') }}/{{$fotografia->url_thumbnail}}" alt="" title="{{$fotografia->titulo}}"/>
                </a>
            </div>
            
        @endforeach
       
        </div>
        {{ $fotografias->links('vendor.pagination.default') }} 
    </div>
</section>

@endsection

