@php 
    $incluirBanner = false;
    $incluirContact = false;
    $incluirFooter = false;
    
@endphp

@extends ('layout')

@section ('contenido')

<header id="header">
    <span>Sebastian Fuentes</span>
</header>
<section id="galleries">
    <div class="ContenedorAdministrar">
        <header>
            <h1>Administrar</h1>
            <ul class="tabs">
                <li><a href="#" data-tag="Todas" class="button">Todas</a></li>
                @foreach($tipos as $tipo)
                    <li><a href="#" data-tag="{{$tipo->nombre}}" class="button"> {{$tipo->nombre}}</a></li>
                @endforeach
            </ul>
        </header>

        <div class="content">
        @foreach ($fotografias as $fotografia)
            <div class="media Todas {{$fotografia->nombre}}">      
                <div class="container">
                    <img src="{{ asset('storage/thumbnail') }}/{{$fotografia->url_thumbnail}}" alt="{{$fotografia->id}}" title="{{$fotografia->titulo}}"/>
                    
                    <form action="/administrar/delete/{{$fotografia->id}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="picture" value="{{$fotografia->url_thumbnail}}">
                       
                        <button class="trash" type="submit"  value="">
                            <i class="icon fa-trash fa-lg"></i>
                        </button>
                        
                    </form>
                    
                    <button id="{{$fotografia->id}}" class="edit" type="submit"  data-toggle="modal" data-target="#editModal" value="">
                        <i class="icon fa-edit fa-lg"></i>
                        
                    </button>                        
                    
                </div>  
            </div>
            
        @endforeach
       
        </div>
        {{ $fotografias->links('vendor.pagination.default') }} 
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-xl " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="" enctype="multipart/form-data">
                        <div style="float:left; width:50%">
                            <input type="hidden" id="id" name="id">
                            <input id="old_fotografia" type="hidden" name="">
                            <div class="form-group">
                                <label for="titulo" class="col-form-label">Titulo:</label>
                                <input type="text" class="form-control" id="titulo" name="titulo">
                                <div id="error-titulo" class="error">El campo Titulo no puede estar vacio</div>
                            </div>
                            <div class="form-group">
                                <label for="descripción" class="col-form-label">Descripción:</label>
                                <input type="text" class="form-control" id="descripcion" name="descripcion">
                                <div id="error-descripcion" class="error">El campo Descripción no puede estar vacio</div>
                            </div>
                            
                            <div class="form-group">
                                <label for="tipo" class="col-form-label">Tipo:</label>
                                <select id="tipo" class="select-wrapper" name="tipo">
                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="fotografia" class="col-form-label">Fotografia:</label>
                                <input id="fotografia" type="file"  name="fotografia">
                            </div>
                        </div>
                        <div style="float: left; margin-left:40px;  width:40%">
                            
                            <div class="form-group">
                                <label for="mapa" class="col-form-label">Lugar:</label>
                                
                                <input id="mapa" type="text" name="mapa">
                                <div id="error-lugar" class="error">El campo Lugar no puede estar vacio</div>
                                <input id="lugar" name="lugar" value="" type="hidden">
                                <input id="lat" name="lat" value="" type="hidden">
                                <input id="lng" name="lng" value="" type="hidden">
                                <div id="mapEdit" style="position:absolute"> 
                                </div>
                            </div>
                            
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="guardar" type="button" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('script')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTOIRO9iDITo9D-IG8q6ouGETHVfx7_ug&callback=initMap&libraries=places&language=es&region=CL" async defer></script>
<script>    

$(document).ready(function() {
    $(".edit").click(function(){
        $('#error-titulo').addClass("error");
        $('#error-descripcion').addClass("error");
        $('#error-lugar').addClass("error");
        $.ajax({
            url: "/adminstrar/get/" + this.id,
            type: "GET",
            data:{ 
                _token:'{{ csrf_token() }}'
            },
            cache: false,
            dataType: 'json',
            success: function(dataResult){
                
                $('#tipo').empty();
                $('#fotografia').val('');
                $('#id').val(dataResult.fotografias.id);
                $('#old_fotografia').val(dataResult.fotografias.url_thumbnail);
                $('#titulo').val(dataResult.fotografias.titulo);
                $('#descripcion').val(dataResult.fotografias.descripcion);
                $('#mapa').val(dataResult.fotografias.lugar);
                $('#lugar').val(dataResult.fotografias.lugar);
                $('#lat').val(dataResult.fotografias.lat);
                $('#lng').val(dataResult.fotografias.lng);

                //mapa
                var lat = parseFloat(dataResult.fotografias.lat);
                var lng = parseFloat(dataResult.fotografias.lng);
                
                var latLng = {lat:lat, lng:lng};
                marker = new google.maps.Marker({
                
                    position: latLng,
                    map: map,
                
                });

                map.setZoom(8);
                map.setCenter(marker.getPosition());
                ////////////////////////

                for (var j=0; j<dataResult.tipos.length; j++){
                    if(dataResult.tipos[j].id == dataResult.fotografias.tipo_id){
                        $('#tipo').append('<option selected="selected" value="'+dataResult.tipos[j].id+'">'+dataResult.tipos[j].nombre+ '</option>');
                    }else{
                        $('#tipo').append('<option value="'+dataResult.tipos[j].id+'">'+dataResult.tipos[j].nombre+ '</option>');
                    }
                }
            }
        });


    });

    $('#guardar').click(function(){
        
        var formdata = new FormData();
        var id = $('#id').val();
        var old_fotografia = $('#old_fotografia').val();
        var titulo = $('#titulo').val();
        var descripcion = $('#descripcion').val();
        var lugar = $('#mapa').val();
        var latitud = $('#lat').val();
        var longitud = $('#lng').val();
        var tipo = $('#tipo').val();        
        var files = $('#fotografia')[0].files[0];
        var error = 0;
        if(titulo.length < 1){
            $('#error-titulo').removeClass("error");
            error = 1;
        }else{
            $('#error-titulo').addClass("error");
        }


        if(descripcion.length < 1){
            $('#error-descripcion').removeClass("error");
            error = 1;
        }else{
            $('#error-descripcion').addClass("error");
        }

        if(lugar.length < 1){
            $('#error-lugar').removeClass("error");
            error = 1;
        }else{
            $('#error-lugar').addClass("error");
        }
            
        if(error == 0){
            
            formdata.append('_token','{{ csrf_token() }}');
            formdata.append('id',id);
            formdata.append('old_fotografia',old_fotografia);
            formdata.append('titulo',titulo);
            formdata.append('descripcion',descripcion);
            formdata.append('lugar',lugar);
            formdata.append('lat',latitud);
            formdata.append('lng',longitud);
            formdata.append('tipo',tipo);
            formdata.append('fotografia',files);

            $.ajax({
                url: "/adminstrar/edit/" + id,
                type: "POST",
                data:formdata,
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                success: function(dataResult){
                    if(dataResult == 1){
                       
                        $('#editModal').modal('hide');
                        //location.reload();
                    }else{

                    }

                }
            });
        }
    });

   
});   

    var map;
  	function initMap(){
        map = new google.maps.Map(document.getElementById('mapEdit'),{
		  center: {lat: 0, lng: 0},
          zoom: 2,
        });
        
        var input = document.getElementById('mapa');
        var searchBox = new google.maps.places.Autocomplete(input);
        var marker = new google.maps.Marker();

        google.maps.event.addListener(searchBox, 'place_changed', function(){

            marker.setMap(null);
            var place = searchBox.getPlace();
            
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            var latLng = {lat:lat, lng:lng};
            var lugar = place.name;
        
            marker = new google.maps.Marker({
                
                position: latLng,
                map: map,
                
            });

            map.setZoom(8);
            map.setCenter(marker.getPosition());

            document.getElementById('lugar').setAttribute('value',lugar);
            document.getElementById('lat').setAttribute('value',lat);
            document.getElementById('lng').setAttribute('value',lng);
        });
    } 
</script>
@endsection

