@php 
    $incluirBanner = true;
    $incluirContact = true;
    $incluirFooter = true;
@endphp

@extends ('layout')

@section ('contenido')
<!-- Gallery -->
<section id="galleries">

<!-- Photo Galleries -->
    <div class="gallery">
        <header class="special">
            <h2>Ultimas Fotografias</h2>
        </header>
        <div class="content">
            @foreach ($fotografias as $fotografia)
                <div class="media">
                    <a href="/storage/{{$fotografia->url_fotografia}}">
                        <img src="{{ asset('storage/thumbnail') }}/{{$fotografia->url_thumbnail}}" alt="" title="{{$fotografia->titulo}}"/>
                    </a>
                </div>
            @endforeach
        </div>
        <footer>
            <a href="/galeria" class="button big">Galeria Completa</a>
        </footer>
    </div>
</section>

@endsection

