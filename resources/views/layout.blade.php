
<!DOCTYPE HTML>
<!--
	Snapshot by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Sebastian Fuentes</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="/assets/css/main.css" />
		<link rel="stylesheet" href="/css/pagination.css" />
		<link rel="stylesheet" href="/css/modal.css" />
	</head>
	<body>

		<div class="page-wrap">

			<!-- Nav -->
				<nav id="nav">
					<ul>
						@if (Route::has('login'))
								@auth
									<li><a href="/logout"><span class="icon fa-sign-out"></span></a></li>
								@endauth
						@endif
						<li><a href="/"><span class="icon fa-home"></span></a></li>
						<li><a href="/galeria"><span class="icon fa-camera-retro"></span></a></li>
						@if (Route::has('login'))
							@auth
								<li><a href="/create"><span class="icon fa-upload"></span></a></li>
								<li><a href="/administrar"><span class="icon fa-pencil"></span></a></li>
							@endauth
						@endif
					</ul>
				</nav>

			<!-- Main -->
				<section id="main">
				
					@includeWhen ($incluirBanner, 'includes.banner')

					@yield ('contenido')

					@includeWhen ($incluirContact,'includes.contact')

					@includeWhen ($incluirFooter,'includes.footer')
				</section>
		</div>
	
	
	</body>
	<!-- Scripts -->
	<script src="/js/app.js" defer></script>
	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/jquery.poptrox.min.js"></script>
	<script src="/assets/js/jquery.scrolly.min.js"></script>
	<script src="/assets/js/skel.min.js"></script>
	<script src="/assets/js/util.js"></script>
	<script src="/assets/js/main.js"></script>
	
	
	@yield('script')
</html>
